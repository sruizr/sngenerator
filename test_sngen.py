from unittest.mock import Mock, patch
import os
from sngen import Indexes, SnGenerator, year_week_prefix, Server, Client
import sqlite3
import datetime


def test_year_week_prefix():
    today = datetime.date.today()
    expected_value = str(today.year % 100).zfill(2) + today.strftime('%W')
    assert year_week_prefix() == expected_value


class A_RepoIndexes:
    def setup_method(self, method):
        if os.path.exists('./test_indexes.db'):
            os.remove('./test_indexes.db')

        self.indexes = Indexes('./test_indexes.db')
        self.indexes.open()

    def tear_down(self, method):
        self.indexes.close()

        del(self.indexes)
        os.remove('./test_indexes.db')

    def should_create_db_file_if_not_exist(self):
        assert os.path.exists('./test_indexes.db')

    def should_get_index_increasing(self):
        index = self.indexes.get('TEST')
        next_index = self.indexes.get('TEST')

        assert index == 1
        assert next_index == 2

    def should_persist_indexes(self):
        self.indexes.get('test')
        self.indexes.get('test')
        self.indexes.close()

        indexes = Indexes('./test_indexes.db')
        indexes.open()
        assert indexes.get('test') == 3
        indexes.close()


class A_SnGenerator:
    @patch('sngen.year_week_prefix')
    def should_generate_sn(self, mock_prefix):
        indexes = Mock()
        indexes.get.return_value = 1
        prefix = lambda: '1234'
        gen = SnGenerator(indexes, 6, prefix=prefix)

        serial_number = gen.generate('test')

        assert serial_number == '1234test000001'


class A_Server:
    @patch('sngen.SnGenerator')
    def should_serve_serial_numbers(self, mock_SnGenerator):
        generator = mock_SnGenerator()
        two_values = {'test1': '1234', 'test2': '5678'}
        generator.generate = lambda key: two_values[key]

        server = Server()
        server.start()

        client = Client()
        serial_number = client.request_serial_number('test1')
        assert serial_number == '1234'

        serial_number = client.request_serial_number('test2')
        assert serial_number == '5678'
        server.stop()

    @patch('sngen.SnGenerator')
    def should_serve_under_stress(self, mock_SnGenerator):
        generator = mock_SnGenerator()
        two_values = {'test1': '1234', 'test2': '5678'}
        generator.generate = lambda key: two_values[key]

        server = Server()
        server.start()

        clients = [Client() for _ in range(3)]

        for client in clients:
            serial_number = client.request_serial_number('test1')
            assert serial_number == '1234'

            serial_number = client.request_serial_number('test2')
            assert serial_number == '5678'

        server.stop()
