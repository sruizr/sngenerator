import socket
import threading
import datetime
import sqlite3
import select
import os
import logging
import time
import sys


logger = logging.getLogger(__name__)
logging.basicConfig(filename='./info.log', level=logging.INFO)


def year_week_prefix():
    """Return string of four numeric caracters 'YearWeek'
    """
    return datetime.date.today().strftime('%y%W')


class Indexes:
    def __init__(self, data_fn=None):
        self._cache = {}
        self.data_fn = data_fn if data_fn else './indexes.db'

    def open(self):
        create_table = not os.path.exists(self.data_fn)
        self.conn = sqlite3.connect(self.data_fn)
        if create_table:
            cursor = self.conn.cursor()
            cursor.execute(
                """create table tracking_index (
                tracking text primary key,
                max_index integer
                )
                """
            )
            self.conn.commit()

    def get(self, tracking):
        if tracking not in self._cache:
            self._load_on_cache(tracking)

        index = self._cache[tracking] + 1

        cursor = self.conn.cursor()
        cursor.execute(
            "update tracking_index set max_index=? where tracking =?",
            (index, tracking)
        )
        self.conn.commit()
        self._cache[tracking] = index

        return index

    def _load_on_cache(self, tracking):
        cursor = self.conn.cursor()

        cursor.execute('select max_index from tracking_index where tracking=?',
                       (tracking, ))
        result = cursor.fetchone()
        if result:
            value = result[0]
        else:
            cursor.execute(
                'insert into tracking_index (tracking, max_index) values (?, ?)',
                (tracking, 0)
            )
            self.conn.commit()
            value = 0

        self._cache[tracking] = value
        self.conn.commit()

    def close(self):
        self.conn.close()


class SnGenerator:
    """Thread safe serial number generator
    """
    def __init__(self, indexes, index_size=4, prefix=None):
        global year_week_prefix
        self.prefix = prefix if prefix else year_week_prefix
        self.indexes = indexes
        self.size = index_size
        self._lock = threading.Lock()

    def generate(self, batch_tracking):
        with self._lock:
            index = self.indexes.get(batch_tracking)
            if index >=  10**self.size:
                raise Exception('Index is too large; batch should be closed')

        index_str = str(index).zfill(self.size)
        return self.prefix() + batch_tracking + index_str


class RecvThread(threading.Thread):
    def __init__(self, sock, sn_generator):
        threading.Thread.__init__(self)
        self.sock = sock
        self.sn_generator = sn_generator
        self._stop_flag = False

    def run(self):
        self.sn_generator.indexes.open()
        while not self._stop_flag:
            rlist, wlist, elist = select.select([self.sock], [], [], 1)
            if len(rlist) != 0:
                logger.info("Received %d packets" % len(rlist))
                for tempSocket in rlist:
                    try:
                        if tempSocket is self.sock:
                            client_socket, addr = self.sock.accept()
                            logger.info('Connected to client socket {}:{}'.format(*addr))

                            tracking_bytes = client_socket.recv(1024)
                            logger.debug('Recieved {} from client'.format(tracking_bytes))

                            serial_number = self.sn_generator.generate(
                                tracking_bytes.decode('utf-8')
                            )
                            logger.info('Sending {} to client'.format(
                                serial_number.encode('utf-8')
                            ))
                            client_socket.send(serial_number.encode('utf-8'))
                        else:
                            client_socket = tempSocket
                            tracking_bytes = client_socket.recv(1024)
                            logger.debug('Received reading from {}:{}'.format(*addr))
                            serial_number = self.sn_generator.generate(
                                tracking_bytes.decode('utf-8')
                            )
                            logger.debug('Sending {}'.format(
                                serial_number.encode('utf-8')
                            ))
                            client_socket.send(serial_number.encode('utf-8'))

                    except socket.error as e:
                        logger.error(e)
                        raise e
        self.sn_generator.indexes.close()

    def stop(self):
        self._stop_flag = True
        self.join()


class Server:
    def __init__(self, ip='127.0.0.1', port=5678, index_fn=None):
        self.ip = ip
        self.port = int(port)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((ip, port))
        self.socket.setblocking(1)
        self.socket.listen(100)
        indexes = Indexes(index_fn)
        self.generator = SnGenerator(indexes)
        # task_queue = queue.Queue()

        self.recv_thread = RecvThread(self.socket, self.generator)
        # self.work_thread = WorkThread(self.socket, task_queue, self.generator)

    def start(self):
        self.recv_thread.start()
        # self.work_thread.start()

    def stop(self):
        self.recv_thread.stop()
        self.socket.close()


class Client:
    def __init__(self, ip='127.0.0.1', port=5678):
        self.ip = ip
        self.port = port

    def get_serial_number(self, tracking):
        cli_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        cli_socket.connect((self.ip, self.port))
        try:
            cli_socket.send(tracking.encode('utf-8'))
            sn_bytes = cli_socket.recv(1024)
            logger.info('Recieved {}'.format(sn_bytes))
        finally:
            cli_socket.close()

        return sn_bytes.decode('utf-8')


if __name__ == '__main__':
    args = sys.argv
    if len(args) > 1:
        args = args[1:]
    else:
        args = []

    server = Server(*args)

    server.start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            server.stop()
            break
